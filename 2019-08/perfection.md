# Perfection

Big glass doors, welcome to our building.
Marble floors and walls, everywhere gilding.
We are grand, important, and lucrative.
The mayor, the news, they build up our narrative.
This building is a reminder of our perfection.
Equity? Profit sharing? Not satisfied with subjection?
All we employ we have shown our divine grace.
And, let us remind you, you can be replaced.
One cannot question the goodness we bring.
Our profits, forevermore, will be on the upswing.
Ignore that squeaky elevator, or that one toilet's weak flush.
These don't dispute our perfection; we have chairs that are plush.
Our employees, our systems, are the best money can buy.
It's just a coincidence that most are white guys.
Yes, behold our intelligence, our accumen, our drive.
We will continue our infinite growth, never a nose dive.
Our investors must be satiated by any means necessary.
That's our business, in this gilded building, out here on the prairie.
