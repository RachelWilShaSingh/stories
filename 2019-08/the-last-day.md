# The Last Day

I peek out the window, pulling aside a thick, dark cover that has been added to all of the office building's windows. We used to have a view of the apartment buildings down the way, the grass and a fountain and a pond, but that was eons ago. I don't know, at *least* a decade, if not more. As everything started dying off, everything inside was business-as-usual, but they put up these sheets so we couldn't see outside. Maybe it would hurt employee morale.

There's no grass anymore, no water, and the apartments are falling apart. The sky is a dirty chalky gray color. We all know what's out there - it's beyond hot and dry and it's definitely hazardous - but I guess when we're here they want us to stay in the work mindset. No time for grief. Just time for creating profits.

I replace the bit of fabric covering the window before anybody notices and I scurry back to my desk. I've been working for about 16 hours straight. There's no reason to go "home" anymore. Just keep working and saving up paychecks as long as I can. Eventually, I won't even have the luxury of working, and then I'll have to figure out how I'm going to survive *out there*.

A low hum is always here in the building, constantly filtering the air to make sure we don't asphyxiate. If we commute to work, we have to be cautious about how long we're outside and what we're breathing. I just live at the office now. I think they prefer it that way, anyway. I spend a little bit of my earnings each day in the company's cafeteria and I just work and sleep. There's nothing else to do. We can charge our phones but there's no wireless signal anymore - that was the last service to go. Internet has been down for *us* for years. What little infrastructure there is left is all horded for the sake of big business continuing to do what they do. There are no TVs in the office, no antennas to get a signal over the air. I suppose there could be television, or maybe some activity over the amateur radio frequencies, but I don't have access to those.

Honestly, I'm not sure what I'll do once they eventually lay us all off. I'm not sure when *that* will be; once they cannot make any more money, period? Once all consumers are dead? Once there are no more resources *to* exploit?

My boss walks behind me and I snap out of it. I'm tired, but I still have work to do, and I don't want to get in trouble. They'll get rid of me if I'm more trouble than I'm worth. I keep working at my computer, writing software to move data here and there. Data seems so pointless. But working is self-defense. Saving money is self-defense. I'll sleep when I'm dead.

-----

"Hey Gary, have you seen John around?" A coworker is walking around looking for our boss' boss, apparently.

"No, I haven't noticed him around today." I respond.

"That's weird, he's supposed to be in on Fridays. I can't find Jack or Jason, either." Akash looks annoyed.

"Important people meeting?"

"Probably." He shrugs and heads back to his corner of the office space.

----

The humming stops. The open floor plan office is eerily quiet without that droning filter noise. I look around and nobody else seems concerned. Maybe the filter will turn back on.

It only takes maybe fifteen minutes for it to start getting dusty inside.

"Hey, Sushil, can we get Gary to report this to maintenance?" I ask my coworker across the way.

"Nobody's seen Gary all day." Sushil responds.

I get up, frustrated at the interruption in my work. "OK let's find some maintenance people."

Sushil, Molly, and I each take different floors to look for someone to help - a boss or a maintenance person - or a phone to call the department on. I tuck my nose into my shirt to minimize the dust I'm breathing in, but it's getting pretty hazy and it's hard to see through.

In the IT department office, I find a phone and pick it up, reading the directory card attached to the base. The phone, however, is dead - no sound. I slam it down on the holder. I'm starting to get really worried now.

Sushil is already back in the main lobby, waiting. "I couldn't find anything."

"Guys," Molly calls after us as she descends the stairs, "there is NO upper management in the building. I broke the door on Jack's door and his books and ocomputer were there, but his family photos are gone." Molly looks really worried.

The first thought that crossed my mind is that Jack got fired, if his personal items were missing, but no - that doesn't make sense.

Sushil, Molly, and I look at each other. Other coworkers have been gathering in the lobby, wondering what is going on. The lights, computers, and electronics all suddenly shut off. I hear yelling from the elevator - someone's trapped inside. There is a heavy tension in the air as the moment lasts forever for all of us. In the corner of the lobby, we all hear one person trying to open the door, but the electronic locks keep it in place. Another forever-moment passes as the same thought occurs to all of us.

My body doesn't work as fast as my mind. In an instant, I'm on the floor as dozens of coworkers run towards the big glass entry doors. I hear the sound of fists on glass as I shield my head and curl into a ball. The yelling and shouting intensifies, only drowned out momentarily by the sound of shattered glass, followed by the thundering footsteps of those still coming down the stairs. The pain of being kicked and trampled overwhelmes the rest of my senses, as I choke on the dusty floor. I extend a hand to try to pull myself, but-
