# Genzies and Millies

"Once, a long, long time ago, there were many groups of people. In that era, there were Boomies, Exxies, Millies, and Genzies.

Boomies had the most power, and were highest in the hierarchy. They treated all other clans poorly, forcing them to work and stealing their value.
The Boomies horded their wealth, and ruled as kings. There was no stopping their dominion over all.

The Exxies were the first to be fooled. They trusted the Boomies when they stated that they would care for the Exxies. They waited patiently for
what they were promised, but it never came.

The Millies were given the same promise. A fledgling clan, they were powerless when the violence started. The Boomies didn't care, though.
The Boomies promised protection, but that promise was hollow. As the Millies grew, there was no wealth left for them - The Exxies managed
to obtain a small portion of the wealth, but the Boomies were still in charge, pulling the strings.

The Genzies were the newest clan. They were given the same promise, but the Genzies saw how the Exxies and Millies were duped.
The Genzies saw the Millies' anger yet powerlessness, and the Genzies were not fooled.

The Boomies continued hording wealth and encouraging violence, and they hid behind their system. They said it was the natural way of things,
as they continued killing the Home.

The Genzies and the Millies knew that the Boomies would not stop until Home was destroyed, and while they held no power in the hierarchy,
they held power together. Both clans knew that they had nothing to lose. Millies reached out a hand and guided and supported the younger clan,
offering the support not given to them by the Exxies or Boomies.

The two clans bided their time, accumulating resources and people-power, teaching and learning, building and growing.

The Boomies just regarded those under them as scurrying rodents underfoot, with no recourse to unseat their power.

Little by little, the younger clans demonstrated their collective power. By supporting each other, they were able to
refuse to serve the older clans, to put a halt to their plots. Without the other clans serving them, the Boomies
had no ability to carry out their whims.

The Boomies threatened all the other clans, took away food, took away medicine, took away homes.
But the three clans banded together, grew their own food, taught each other medicine, and built new homes.

The tall glass and marble towers the Boomies had built to honor themselves crumbled from disuse,
the Boomies not willing to lift a finger to do what they asked of others.

Angered, they resorted to violence. Their weapons more powerful than anything afforded the other clans.
Those who fought the hardest were shielded by those with greater privilege in the former hierarchy.
While there was pain and there was death, to not stand against the Boomies meant death for all clans.

Having lashed out, the Boomies' last resort was to wage war on the other clans, and the other clans accepted the challenge.
The Boomies were weakened and did not hold the power or the resources that they once had, and the Exxies, Millies, and Genzies
rebuilt their own lives independently and did not rely on Boomies for necessities. Even with greater weapons, the Boomies eventually
fell, for those doing the fighting in their place were not also Boomies; they would not pull their own weight.
Through deaths or desertion, the Boomies lots all their servants, and the three clans only grew stronger together.

Their liberation from the old hierarchy meant that they could build their societies around taking care of Home and each other.
Plenty of sacrifices were made, but a new balance was in place. A lesson had been learned in the most painful way possible,
with Home damaged irreparably in some ways. The clans were weary, but tried their best to continue on.

Step by step, they tried to heal and teach. They tried to guard against the same mentality that had led the Boomers to
reach for and take power over all else. At some level, all you can do is try."
