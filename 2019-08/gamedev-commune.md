# Gamedev Commune

"Two hours, fifteen minutes, worked on collision detection between player and terrain." Kara logged in a spreadsheet. She folded her laptop and put it in her bag. She was at a good stopping point, she thought, and decided to take a break.

Outside of the common house, the sun was shining and the sound of wind blowing through the cornfield filled the air. Some of the other indie developers were inspecting the corn and other crops for parasites, to try to figure out if they needed to intervene in case any bugs found their crops tasty. But Kara had already finished her duties for the day early in the morning and now had the rest of the daylight for herself.

In the kitchen, one of the other developers had put out a big bowl of lentils soaking in water in preparation to be cooked later in the evening. Someone else was doing the daily sweeping. Any developers who didn't have chores today or who had already finished were working on their games or relaxing and enjoying the beautiful day.

Kara sat down next to Jyoti and plugged in her laptop to charge, powered by some solar panels on the roof of the kitchen and every other building in the commune. Jyoti was working on game art for a joint project her and another developer, Ben, were working on.

"How's your game going?" Kara asked.

Jyoti was deeply focused on what she was doing. "It's... fine. Just... cleaning up... the spritework..."

"I'm trying to get my game physics all done by Sharing Day this week. I'm hoping it's all playable so I can get good feedback from the others."

"Yeah... If you want me to play it... let me know a little later..."

"Sounds good. Thanks, Jyoti."

Kara left her laptop at the table to charge while she wandered off to find the newbie. Kallie had just moved into the commune and was probably still exploring everything. Kara looked at the chore list on the wall of the kitchen... Looked like Kallie was up for some knitting.

Kara wandered into the craft house where several people were mending clothes and Kallie was knitting her own blanket. The commune had been thinking of raising sheep to add to their resources, so that they wouldn't have to go into town to buy yarn with their community dues (some amount of their game sales, contributed into a pool to buy equipment and supplies for the group), but that would require raising a barn and buying sheep and there just wasn't enough community support for that yet - not just for some knitted goods.

"Hey Kallie, how's knitting going?" Kara asked.

"Oh, it's fine. This is kind of hard..." Kallie responded.

"That's OK, everyone's first blanket is lumpy and weird. You can always make a new one later on."

Once the autumn rolled around, any newbies who still had holes in their blankets, or had made their blankets a little too small, usually spent a day or two re-doing their work. They would unravel their first-attempt blankets, darn it into a ball, and make it again. Since some of the chores included knitting and crocheting, they were usually better at making their blankets the second time around.

Back at the dormitory, Kara made her way to her room. She had painted it orange and brown, and her own knitted blanket was the color of a bright red autumn leaf. Each room was big enough for a bed, a small computer desk, and a closet to store clothes and items that weren't currently in use. Most people didn't spend their free time in their rooms unless they really needed quiet to focus, or if they needed privacy for some other reason. The rest of the grounds had anything else they would need.

The sun wasn't fully set yet, but Kara closed her curtains and laid down. Tomorrow morning was her day to feed the chickens.
