# Old Man Island

(Just imagine that these old people are boomers, but there's like
life-extending technology. This short story is inspired by my mom.)

-----

"...Is what again?" Mrs. Franklin shifted in her seat.

"Well, that's just one option for our Twilight-Life care." The
salesman pushed his monitor a bit to the side so that she could
see his screen. "Now we've already gone over digitizing your
brain to spend your T-Life in any kind of setting you'd like.
I'd actually recommend it over", he gestured to the images on
the screen, "what we call 'old man island'."

Mrs. Franklin adjusted her glasses and looked at the screen.
It was, indeed, an island of old men. *Very* old men.

"...Are there just men there?" she asked, not even noticing that
it was specifically made up of only old *white* men.

"Well, you see," the salesman gathered his thoughts a moment,
"ever since the medical advances to allow people to indefinitely
postpone death, and with quality-of-life enhancing services,
many men - with the funds - opted to just take that route."

Mrs. Franklin pondered a moment... *that sounds horrible...
I don't want to spend an eternity with my husband...*

The salesman noted her grimace. "A lot of, uh, housewives from
about your generation, though they could have afforded -
well, they tended to opt for virtualized T-Life, or uh,
the natural way out."

"I can imagine." What good was "'til death do us part" if there
were no death to give her a rest?

"Honestly, though, it's become kind of, er, insular on the island...
really, I don't think they'd allow a woman on even if she wanted to...
they, uh, don't really like what they call 'outsiders'."

Mrs. Franklin wondered for just a brief moment what they do in their
secluded fabricated island afterlife on their own, but only for just
a split second. Then it occurred to her that she really didn't care.
Let them have their silly island away from normal life. At least
they didn't have a hand in politics anymore - legislation had passed
stating that T-Lifers could no longer vote and, as they were living
somewhat "artificial" extended lives, were to spend their T-Life
physically separated from the rest of society.

"Tell me more about that virtual world with the big library in it."









