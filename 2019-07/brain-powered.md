# Distributed Computing over Mind

Life has improved since I signed up to lease a portion of my brainpower as a Distributed Computing over Mind service.
Though there were several DCoM providers I could have signed up with, I decided that Amazon's had the most bang for the buck.
It was difficult getting by, paying my bills and all that, and I needed some extra income, so hey, why not?
I don't use all of my brainpower, anyway.

Big Data is where it's at these days, and while computers keep getting faster and faster, nothing can compare to the human mind.
That's why the big three companies have set up an additional service that goes along with their cloud computing services.
Those of us with extra mental processing time to spare lease out our brains to deal with crunching all that data.
We lease to Google or Amazon or Facebook (or maybe Elon Musk if you're REALLY adventurous!) and they, in turn, sell a batch
of brain-processing-time to clients. Probably for 10x the price of what they pay ME, but hey... I don't have the kind of platform
to lease my brainpower directly to people!

I don't even have to go anywhere or really do anything! They came out to my apartment to install the hardware into my head.
Now, any spare brainpower I'm not using is used to make ME money! It's amazing!

----

It's been about a month since I started leasing out my brain processing... thingies. I don't really notice it while it's running,
just whatever spare brain cycles go towards that stuff. I think it mostly runs while I watch TV because I like to veg out after
the end of my shift. Too tired, mentally and physically, to do much after work anyway. So, it's good that I can keep earning money
while I'm being idle.

I've mostly used the money for paying for all these streaming services. Dang, when I was a kid there were so many TV packages
and it was so expensive. Then Netflix came around and things got better. Now everybody's got their own streaming services again.
And of course each has their own "killer shows", and I want to stay up-to-date on them so Twitter doesn't spoil 'em for me.
So like, I have this gigantic bill now. Ugh. But I don't really have any hobbies right now because work tires me out so much.

----

Lately it's been hard to focus, like there's a fog in my mind. I wonder if it's the brain thingy? Or just my tiredness from work.
Like it literally feels like a blob is in my brain taking up space, so I can't think proper.

Just more TV, since it's so hard to focus. I don't really have to problem solve when I watch TV. I don't have the energy to
do anything else.

----

I think I'm officially spending more on streaming than I am earning from the brain thing. I tried to contact Amazon to cancel
it but I couldn't find a way to contact them? But there's too much text on the website. It's too hard to read when I can't focus.
Maybe I should cancel these streaming services and just quit my day job.

----

I don't have a job now. Or, not my day job. I have the brain thing. Mostly, I sleep now. Too tired for anything else.
But more money has been coming in. And I'm not watching TV because I'm sleeping so much. So I'm saving more. That's good.
OK, I'm tired now. Gonna sleep more.

