If I put my sweater on, I can burn a few minutes.

If I check my phone, I can burn a few minutes.

If I scroll through this code with unfocused eyes, I can burn a few minutes.

If I pretend to be making notes in my day planner, I can burn a few minutes.

If I clean up my desk, I can burn a few minutes.

If I dump out my drink, I can burn a few minutes.

If I go to the bathroom, I can burn a few minutes.

Let's count the minutes

as my back pains

and my brain goes elseware.

I've done plenty of work.

I'm mentally done for the day.

But it is not yet 4 pm.

So I must find more ways to burn a few minutes.