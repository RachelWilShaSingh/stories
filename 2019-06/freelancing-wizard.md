Ccrreeaakk. 

Dama reached into the rusted mailbox and pulled out this morning's letters.
Shuffling back into the house, she began sorting through them into various piles.
She opened the only letter addressed to her while Alehale poured a cup of coffee and
joined Dama in the livingroom.

"Got anything?" Alehale asked. Her hair was unbrushed and tied up as she had just woken up.

"Just one job..." Dama read the details, "De-bugging job."

"Oh. Is that going to take long?"

"Depends on how badly someone messed up their spells." Dama folded up the letter and put it in her bag.
She put on her "professional" wizarding clothes - the standard button-down robe in a neutral tone -
grabbed her bag full of utensils that she *might* need, and headed out.

----

Dama pulled open the heavy wooden door on the shabby tavern. No patrons were drinking yet, but it was still
quite early. Nobody was even at the counter.

"Uhm, excuse me?" Dama tried to call toward the back of the building. A pale, balding man came out front.

"I'm here to help with your bug problem."

"Aah, right. Come on." The man waved her into the kitchen area, crunching bright purple roaches underfoot.

"So what lead up to your problem here, mister...?" Dama spoke as she stepped lightly; she did not (purposefully)
step on any of the magical bugs.

"Oh, oof. I'm Griz. Uhh." Griz rubbed his forehead, "So for a while there, we were getting more patronage, right?
And I've been running this place myself, with a little help out front.

Well, some of these other places, they've got enchanted helpers, right? Ghosts 'n' stuff? To move food here an' there,
helping out. Cheaper than hiring more people..."

"And so you hired someone to summon some help?" Dama inquired.

"Well I tried this out myself. Got a book from the library, thought I could do it. I'm a small business owner, you know,
so I have many hats an' all."

"Ahh, I see." Dama said, trying her best to not show the annoyance on her face. Often, any underlying magical wonkiness to
clean up is the result of untrained wizards, or just not wizards at all, trying to get a job done.

"This won't take too long, luckily it was just a small spell." Dama assured him.

"Ok, that's good. What do I need to do?"

"Well, nothing, really. I'll take care of it. I should be done before you open this evening."

"Good, good. Now actually, ah, can I watch? I'd like to see how to do it myself."

Dama bristled, but maintained her composure. "Sure, but it might be boring."

Dama knew she'd be back to de-bug another misuse of magic here in the near future.

----

On days Dama worked, she liked to be lazy afterward, so she went straight home to
wrap herself up in blankets and read a book. Alehale came home much later in the afternoon.

Alehale had a regular job, receiving complaints about various types of wagons built by a local wagonsmiths 
(or whatever it's called),
diagnosing the cause of those complaints, and relaying that information to the builders so the issues could be fixed.

Dama had graduated from the kingdom's wizarding school, but had not been successful finding a
niche for herself in the school as well. Now, she just took odd-jobs to make ends meet.

Being a wizard really wasn't as glamorous as everyone had made it seem when she was a child.
For one, most people don't *need* dragon killing spells, or world shattering spells. Most of the
magical work Dama did was just *fixing other peoples' mistakes*, or moving peoples' inventory around
with basic levitation, or just mending little things here and there through her magic.

It was not that riveting.

----

"What if we just opened our own tavern or something? I'm so sick of magic." Dama was leafing through
a book about the languages of common familiars, laying on the floor with her cat on a fuzzy blanket.
Alehale was sketching something while sitting at the table.

"But we don't have any money." Alehale reminded.

"Yeah, I know..." Dama shifted as she started reading the next page, trying to find a comfortable way
to read so that her arms wouldn't fall asleep. 

"That tavern owner didn't know what he was doing,
but still has the money to own land and property and run a business. He tried to magic himself some 
helpers because he was too cheap to hire any help, or to hire a proper wizard." Dama rolled her eyes at herself.

"Wish I had the option to not be cheap. How do we have *negative* money?" Alehale grumbled.

"Maybe we should just abandon everything and go live in the forest, eating bugs and pooping outside.
Pay no bills and stuff." Dama suggested.

"That might work if there were still any common land."

"Ugh, yeah, all the land is taken. Can't even just exist. Life."

"Life."
