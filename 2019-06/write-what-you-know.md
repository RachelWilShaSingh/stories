Cody stared blankly at the screen. They had begun writing code for an integration test, but it
was Friday afternoon, two hours until happy hour, and Cody's concentration was gone. They tried,
and they did get some work done, but their anxiety built as they thought.

"Everything is due early next week because of the holiday... If I don't get more tickets finished,
then I'm going to be miserable next week."

Still, for all the self-guilting, Cody continued staring at the computer.

Cody twiddled with one of the fidget toys they had on their desk. It was oddly satisfying. But still,
it wasn't helping.

"I feel like being creative," Cody thought to themself, "but, even if I were going to secretly work
on something here, I wouldn't know WHAT to work on." Cody turned their head slightly, partially to look
at their day planner, but also to check whether their cube-mate was at his computer. He was.

"Even if I had time, I wouldn't know what to do..."

Cody's anxiety kept building as their thoughts continued pacing inside their mind, like a caged tiger
at an old-timey zoo.

"I'm too tired, I focus too much on work, I don't have enough time in the day...
I just want to DO something."

Cody started cracking their knuckles (and other joints in their hands), another way they dealt with the anxiety.
Soon, their wrist started hurting from repeated bending and pulling and cracking.

"Maybe I can type a story... Typing a story sounds like writing code, right? That's the most discreet..."

But Cody didn't have much experience writing stories, and they couldn't think of any story ideas
through the mental fog that their job created in them.

"Write what you know?" Cody pondered. Cody's concentration wouldn't stick around. All they could
think about is their lethargy and frustration and weariness.

Cody opened a minimal text editor that easily blended in with their code.
