

-----

"Ĉu mi ankoraŭ vivas?" la Klerikino miras al ŝi mem.

Malrapide, ŝi sentas la aferojn ĉirkaŭ ŝi. 
Ŝi sentis la luladon de la maro,
la brilecon de la ĉielo,
la odoro de la marsalo.

Ŝi faris juĝan eraron, kaj ŝi anticipis morti ĉar ĝi.
Ŝi neniam antaŭe spertis koleron tiel, 
kaj en unu momento ŝi ekbatis la Varlokon.

Kaj li sorĉ-batis ŝin reciproke kaj forte.



Lia voĉo respondas al ŝi super la sono de la maro.

-----

En la paco de la nokto, ŝi rigardas la plafonon fikse.
Ŝia menso ne dormas, kaj fakte kolerigas ŝin.
Ŝi ne bedaŭras ŝian atakon al la Varloko;
li meritis la baton, kaj ŝi ne intencis morigi lin,
sed ŝi ne certis ĉu la Varloko intencis mortigi ŝin aŭ ne.

Ne, evidente. 

Sed, kial? Li estas malvirtulo, ĉu ne?
Li facile povas mortigi ŝin ĉar ŝia eraro, sed li ne faris.
Li venkis ŝin, tamen li ankaŭ resanigis ŝin kaj alportis ŝin al la remboato.
Li ne forlasis ŝin.

*Ne pensu pri.*, ŝi postulas al ŝi mem; *Kial ĉagreni? Li ja estas malvirtulo.*

*Jes, sed, ni kvar vojaĝas kune. Ni estas grupo, por nun.*

*Vi nur ĉeestas ĉar via dio volas vin venku la malvirtulojn.*

*Se la dio volas tion, kial li donas tiun ĉi grupon al mi? Ŝajnas ke ni ne faras bonagojn.*

*Kie vi venkus malbonulojn? En la preĝejo? Aŭ proksime al la malbono?*

La Kleriko turnas en la lito de la taverno, 
sed la vando de la ĉambro ne distrigas ŝin el la sono de lia voĉo en ŝia menso.

-----






