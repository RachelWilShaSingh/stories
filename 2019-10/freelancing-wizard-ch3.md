Dama slumped against the wall of the Freelancers Guild, waiting for the discussion to end. A new guy had come to the guild to find work. Most of the guild guys Dama got along with just fine - not really *friends*, per se, but peers in magic. This newbie decided to introduce himself by telling Dama about the last guild he was part of. Dama stood there and listened politely, wanting to really get her next job and continue on with her day.

"So, clearly the guild had something against *me*. I was one of the few *white men* in that Freelaners Guild, and I wasn't getting any of the *good* jobs."

Dama's face remained blank, but her brain was lighting up. "Red flags, red flags!"

"I had to complain to the town officials. The guild had mostly women in it, mostly people of color, and the deck was unjustly stacked against me."

Dama had thought this new guy looked as plain as a saltine, but she didn't realize he'd be *this bad*. She wondered to herself if perhaps this newbie knew the other difficult guy - Emetha - it seemed like they would be buds. Dama cursed under her breath; just Emetha was bad enough. *Why was the guild letting these clowns in?*

Dama had no response for the man, and excused herself to get the next daily posting for a gig. "Dear Lushede, I hope I don't see that guy around too much." she said in silent prayer.

-----

Dama returned to the Duthahoth she had already worked at - they always needed more help. She put her things down beside a chair and table and began doing her work, when a chair slid out beside her. She looked up--

"FEK." She thought to herself as she clenched her teeth.

"Oh hey. It's you again. My name is Bremeda."

Dama had an entire conversation in her head in the next few seconds, basically boiling down to "YOU SHOULD HAVE KNOWN. You need to start showing up later so you can choose where to sit. You might get assigned the same jobs."

Dama was somewhat sure that the *correct greeting* would have been to flip the table and throw a glass vial at his stupid head, but she wasn't just out in the world - he was in the guild, and at the same job as her. She couldn't figure out what to do, so she simply gave her name and otherwise sat in uninterested silence.

"I swear if you try to talk to me..." she seethed silently.

-----

The Duthahoth had a lot of work, and Dama and various guildmates came and went on various days. Dama kept herself around a couple of mages she got along with - Zhorumi, a sound mage, and Rahu, a healing mage. On one occasion she was careless in where she sat and Bremeda sat next to her, her skin crawling. But after that she stayed diligent, not engaging with him. She had enough of a headache dealing with Emetha, and the guild wasn't doing anything about him picking on Dama.

She spent time daydreaming about leaving the guild while Emetha was there, and giving her farewell to him in the form of the middle finger. But at the same time, she wasn't sure how that would affect her freelancing career. She grumbled about not being able to get ahead *and* having to deal with this shit.

-----

Dama resigned herself to her temp-desk, slouching forward as her robes enveloped her shrinking form. She was finding it hard to concentrate as her inner-mind struggled to free up room to just *be itself*, avoiding the work she was now doing.

"Wouldn't it be nice if I could just split myself into two, one for work and one for more fulfilling, creative endeavors?" she wondered to herself. But, she felt a pang of guilt at the idea of being stuck as the *working half*, doomed to only labor without the enjoyment afforded to the hypothetical other half. But, as it was now, she was effectively the working half anyway, with the creative sparks coming and going but never kindled - there was no time to nurse and raise that fire. So, she stamped it out whenever it appeared, and continued on with her spells for the day.

-----


