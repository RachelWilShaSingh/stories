# Esperanto stories

## La programisto

Unue, programisto sidas cxe labortablo, cxe la oficejo. La programisto havis tri biletojn por novaj funkcioj.
Tamen, la programisto bezonis liston de la specifiaj funkcioj, kaj gxis tiam, ri ne povis programi ion ajn.

La programisto rigardis la cxambro - ri kaj tri aliaj programistoj sidis en la samcxambro, kaj cxiuj laboris.
Nia programisto pensis... ri devis labori!

Kaj post tio, la programisto ekkonsciis - oni simple bezonas aspekti labori!

Do, la programisto malfermis *etan etan* panelon en ria medio, kaj kreis dosieron por rakontoj -- la sono de rakontskribado sxajnas simila al programado, cxu ne?

Kaj la programisto ne programis tiu tage, ri verkis rakontojn.

## Edgaro la Kato

### La Livero

Edgaro rigardis la konstruajxojn cxirkauxe de li; "*kie estas gxin?*" li pensis al li mem.
Li marsxis cxi tien kaj tien, kaj sercxis por lia celo en la malluma nokto.
La aero malvarmetis kaj Edgaro ne alportis lian jakon.

Finfine, li vidis la domo: 13457 Morturo Strato.

Li anaspasxis al la pordo, kaj malforte frapis sur la pordo.
Li atendis kaj atendis, kaj nenio okazis. Li frapis denove kaj atendis denove.

Gxis kvin minutoj, li auxdis ion. Liaj katoreloj rotaciis al la sono antaux lia kapo rotaciis.

"Neniu logxas tien" diris maljunan katon.

"Cxu vere?" Edgaro demandis, "Mi ricevis peton por mangxajxoj el tie."

"Hmm", la maljunulo pensis, "eble estis *fantomo*!" sxi sugestis.

"*FANTOMO?*" Edgaro demandis. "HO, VE!"

Edgaro tremetis kaj la maljunulo rigardis lin bonhumore.

"Kara junkato," sxi respondis, "fakte ne ekzistas fantomojn. Mi blagis!"

"Nu, nun mi ne tute certas ke vi diras la veracon!" Edgaro vekriis.

BUUM!

Iu krakis malantaux Edgaro, en la forlasita domo. Edgaro rigardis la domon, kaj la maljun--

Sxi ne jam estis tie! Pli rapide ol fulmo, sxi malaperis!

La piedoj de Edgaro nun estis sxtonoj; li komandis lin mem foriri el tie, tamen ili ne obeis! 
La aero malvarmegigis cxirkaux li, kaj li frostortremegis. 
El la domo, ombro aperis kaj atendis - eble gxi rigardis Edgaron? 
Edgaro rigardis la ombron ankaux, kaj gxis du momentoj, lia vido blankigxis...

### La domo





