# Ducks

"Why do you always write about ducks?" She asked me, puzzled.

"Because it's hard to think about anything else." It's true, all my stories are about ducks.

"It was cute the first handful of times, but I'm getting bored of ducks."

"I mean, me too, honestly. But my brain doesn't have any more space left over for anything not-duck-related. I think about ducks all the time. That's all I can think to write about."

She stared blankly at me, possibly trying to decide whether I was joking or not. I wasn't.

"I don't know, come up with an imaginary world or something."

"I have, but it has ducks in it."

"NO NOT WITH DUCKS."

"Ducks consume my life."

"WHY?"

"It's my job to care about ducks. And ducks take up all my time. I spend time with ducks all day, then at the end of the day I recover from spending time with ducks, so that I can do it again tomorrow."

"THAT DOESN'T MEAN YOU HAVE TO WRITE ABOUT DUCKS."

"My entire life is ducks. I don't know what to do."

"MAYBE GET A JOB THAT DOESN'T MAKE YOU OBSESSED WITH DUCKS."

"But I have... *BILLS* to pay! (Hyuck hyuck)"

"OH. MY. GOD."

"But for reals, duck jobs are the only thing I'm qualified for. If I took a non-duck-job, I'd have to take a significant pay-cut."

"But then you wouldn't have to think about ducks all the time."

"Yes, but who knows what other jobs require you to think about all the time. Maybe chickens or hummingbirds."

"Not all jobs revolve around birds."

"All the jobs I can do, are."

"JUST. CHEESUS CRUST. STOP."

"I'm trying, honestly. But I'm not sure how to *not* think about ducks. I figure it will stop when I finally cover my *DOWN* payment on a house, then I can switch jobs."

"Ugh do you have any more duck puns?"

"Nah, honestly I'm just *WINGING* it."

"..."

"Honestly, even though I'd like to *FLY THE COOP*, but I'm not yet ready to *LEAVE THE NEST*."

"Ducks don't live in coops."

"Until I pay off my *BILLS* --"

"You used that one already!"

"-- I will be in a perpetual *FOWL* mood."

"You're like a sitting duck for depression."

"Honestly, MOOd."

"Uh, are you trying to pun cows now?"

"Yeah, I ran out of duck words."
