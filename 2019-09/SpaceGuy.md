# Space Guy

`AGENT BOB, INCOMING MESSAGE.`

Bob twirled around in his rotating spaceship chair, pulling himself away from his Space Paperwork to answer the Space Phone. He rolled himself over to the wall and took the Space Phone off the Space Receiver.

"Yyyelllow?" He said casually.

"Agent Bob," the familiar voice of Bob's boss, Mr. Potato, grumbled through the phone, "we have just received a report of a Space Rogue causing havoc on a middle world."

"Aw dangit, Tater, I'm not finished with the paperwork from the LAST mission." Bob protested.

"How long will it take you to finish THAT up?" Mr. Potato inquired grumpily.

"Probably another day or so. You always give me the messy missions."

"You're the best agent we've got for Space Cleanup, agent."

"Oh you flatter me."

"Now finish that *$#+ paperwork and get your butt to Planet Florpin, ASAP!" Mr. Potato slammed his Space Phone down, transmitting a loud SMACK over the Space Line and into agent Bob's ear. Bob winced and slammed his own Space Phone down, knowing that Mr. Potato would not be able to hear it.

Bob returned to his Space Desk and continued with his Spaperwork as the new set of briefing documents printed out.

----

![Porp](images/Porp__01.jpg)

```
PLANET: FLORPIN
CITY: LORP
INHABITANTS: PORPS, A RACE OF BIRD-LIKE ALIENS
INTERNATIONAL AUXILIARY LANGUAGE: ESPERANTO

MISSION DATA: A RESIDENT PORP BY THE NAME OF
 JORMMY HAD BEEN ARRESTED BY LORP AUTHORITIES
 BUT MANAGED TO ESCAPE CONFINEMENT. NO
 DIGGING, NO FOULPLAY, IT APPEARS THAT JORMMY
 WAS ABLE TO STROLL OUT OF THE HOLDING FACILITY. 

POSSIBLE SCIENTIFIC, PSYCHIC, OR NANO
 EXPLANATION.

AGENT'S TASK: DETERMINE CAUSE OF ESCAPE,
 INCAPACITATE JORMMY FOR FURTHER
 INVESTIGATION.
```

-----

When Bob's Space Ship, the Pokey Pike, landed he was greeted by a city representative and a bicycle.

"That's my transportation?" Bob asked the representative, nodding towards the bike.

"SQWOORK?" The Porp responded.

"Oh, oops." Bob remembered that his Esperanto was needed here. "Ĉu mi uzos tiun biciklon?" (Will I be using that bicycle?)

"JES JA, SINJORO BOBO." (Yes indeed, Mr. Bob) The Porp responded. "NI HAVAS ĈIOM DA LA HOMAĴOJ ONI BEZONUS." (We have all of the human-things one would need.)

Bob stared blankly at the bike. "Uu, kie estas la kapitalo?" (Where is the capital?)

"PROKSIME 100 MEJLOJ." (About 100 Miles)

Bob wondered about how much the Porps actually knew about humans.

----

After a lengthy bicycle ride with the Porp representative flying beside him, they finally arrived at the capital building. Though the building was many stories high, there were no stairs or elevators within. 

The representative Porp had to grasp Bob with its talons  and fly him up to the top floor. There, Mayor Rornda was perched at her desk, pecking at her paperwork.

They spoke in Esperanto, but I'll write in English because most of you probably don't know Esperanto.

"Ahh, agent Bob, thank you for coming on short notice." Rornda bobbed up and down as she spoke, regarding Bob out of the eye on one side of her head, and then the other on the other side.

"What leads do you have for me?" Bob huffed out, still exhausted from the bicycling.

"Jormmy left no trace and no apparent evidence in his escape. As far as we can tell, there were no accomplices, no bribes, no blackmail. It's as if he weren't ever in the building." Rornda shifted left and right on her desk, making little squeaks between sentences. "We have enlisted one of our own to guide you around the city and answer any questions you have, although we have all the accomodations a human could possibly need."

Bob put a hand on one of his shoulders, which was still stinging from the ride up. The representative Porp couldn't help but scratch the hell out of his back as it carried him up. "Ok, well, how do I get back to the first floor?"

"Oh we have a climbable wall for visitors who cannot fly."

-----

Bob's hands stung. The way down was basically a rock climbing wall, but less fun. There were pegs here and there for holding onto as one climbed up or down the wall, but there was no safety harness or anything. At one point, Bob had misjudged a foothold, lost his balance, and skidded part-way down the wall, his hands constantly grasping for SOMETHING to hold onto. He did finally grab a sturdy enough peg, after thoroughly scraping the crap out of his hands on the wall itself. This was NOT how he imagined dying.

"HELPU MIN!" Bob demanded, clutching two pegs for dear life, unable to move himself another inch. "SOMEONE? CAN SOMEONE GIVE ME A RIDE DOWN?" 

It felt like an eternity. Bob sat there, aware of his palms perspiring, his legs shaking. Surely he was about to die. This was it. And no beneficiary for his life insurance! It was insurance through the government and, for an agent like him, it was pretty much the best life insurance you could get. 

He squeezed his eyes shut and leaned against the wall, its cool stone surface quickly turning warm from his touch. His hands were burning, sore, and moist. His head was pounding. His heart racing. His butt clenched. Then something whizzed past him in a feathery fury. He was caught off guard and fumbled his grip, slipping once again, and this time falling backwards away from the wall.

FFFFUUUUU---

He stared up at the quickly shrinking ceiling. In that eternity he debated with himself whether it would be better to keep his eyes open or shut them. Whether to try to twist in the air to face the ground, or keep facing the ceiling. Or maybe if he landed on his feet he could narrowly survive? He imagined an appropriate soundtrack to his demise. Probably something with piano, very dramatic, maybe a woman's voice ringing on top of the melody. Mournful.

But then the thud he felt wasn't what he had expected. It was soft, downy, even, and the ceiling continued shrinking but at a slower pace. He had landed on the same Porp that had made him lose his grip.

----

![Ornna](images/Ornna.jpg)

Finally reaching the ground, Bob slid off the back of the Porp and laid on the straw floor. He was not ready to deal with gravity anymore right now.

"Dankon, amiko." (Thank you, friend) Bob sputtered, looking up at the Porp as he made a straw angel in the floor.

"Do you need a doctor, sir?" The Porp responded.

"Oh, maybe." Bob thought. "Or at least some rest. Do you have any non-bicycle vehicles I could use to get to a hotel?"

"Uh, no... Why would you need something besides a bicycle?" The Porp chirped.

Bob groaned and shut his eyes briefly. "Look, uh. What is your name?"

"Storphen."

"Storphen, would you be so kind as to grab my shoulders and *fly* me to a doctor? I don't think I could make it on a bicycle."

"Ummm." Storphen thought a moment, looked over Bob, and looked at the door. "Look, I have a lot of work to do..."

Bob lazily raised one hand and made a "go on" gesture. Storphen waddled out of the building and Bob stayed still, his eyes closed.

He was awakened by chirping that belonged to a different Porp.

"Sinjoro Bobo?"

"Jes?"

"Mi nomiĝas je Ornna." (My name is Ornna) "I am your assistant for this case."

Bob laid there in silence, possibly having dozed off again, and Ornna nudged him back awake. Bob startled and finally opened his eyes.

"I'm Ornna. I work with the Porp Police and I'm here to guide you around Lorp."

"Oh, good. Sorry, I'm a bit worn down right now. That...er, *climb* took a lot out of me." Bob stared past Ornna, up at the ceiling, waaaay up high.

Ornna's feathery face hovered over Bob, her sky blue feathers a welcome change from the gray walls of the building. Her cheek feathers were pink, giving the illusion of blushing, though she was plenty collected.

"Mr. Bob, let's get you to the ambassador suite we have prepared for you, it looks like you need a rest."

"No more biking." Bob protested.

"I'll carry you, sir."

---- 

When Bob got out of the shower, Ornna was perched out on the patio, waiting patiently. Bob was pleasantly surprised to find that the Lorp Porps had brought his things from the Pokey Pike to the suite, and after drying off he put on a pastel suit, trying to match the bright colors of the Porps.

There were no doors in the suite, possibly because having two wings and two talon-feet made it somewhat impractical to deal with such things. Instead, line of sight between different rooms was split up by diagonal or curved hallways, keeping the rooms accessible but maintaining some visual privacy.

Bob came out to the patio and leaned against a perch next to Ornna.

"When can we go investigate the holding cell?"

"It's getting late, so we can go in the morning." Ornna chirped. "Until then, if there is any place you would like to visit or any accomodations you need, please let me know."

"I'd like to explore Lorp some."

-----

![Porp language Solresol](images/Solresol.jpg)

![Chef](images/Chef.jpg)

Bob insisted on staying on the ground as much as possible as he strolled along the dirt-and-straw streets of the city with Ornna hopping beside him, trying to politely match his pace. There were a few buildings accessible at ground level -- the hospital (perhaps in case a Porp hurt their wing and needed to get in), a school for young Porps, a few stores here and there -- but many buildings were up high; either built on top of a ground-floor structure, or sitting atop columns without a ground floor at all. Clearly, the city was built for flyers and didn't have enough "grounded" guests to really bother changing anything.

"Next time," he thought to himself, "I'm going to demand they buy me a small flying shuttle craft that I can use to get around and to rest in."

After a bit of aimless exploring, Bob's stomach began protesting. Up until now, it was still too twisted into knots to bother being hungry, but the cool air and earthy smells of the outdoors helped calm his nerves.

Ornna thought for a bit about available dining (no birdseed, Bob said) and eventually Bob to let her take him to a restaurant up high, serving "exotic foods from other worlds". The building was woven together with sticks and straw of varying colors, and the only signage outside was a small clay tablet, with a set of dots inlaid (possibly molded with a beak?), the dots being at varying vertical and horizontal locations, almost like notes on a musical staff.

"This place is called DO-SI-SOL-SI." Ornna said, singing the restaurant name as notes (do-re-mi-fa-sol-la-si-do) rather than saying "do, si, sol, si".

The only Earth items on the menu were Corn on the Cob and Peanut Butter... That was it. There were dishes from other planets as well, but Bob assumed they were all just as Bird-centric. He filled up on corn.

As Bob and Ornna sat discussing Florpin after their meal, a pudgy Porp waddled out from the kitchen and came over to their table.

"Ĉu la manĝaĵo plaĉas al vi?" (Are you pleased with your food?) the Porp asked.

Bob didn't want to be rude, but the corn was just steamed corn, no salt or butter, and there weren't many appetizing options available. The Porp tried to interpret the look of dismay on Bob's face, but this was his first time seeing a Space Alien and had no idea what it meant.

"It was... Okay." Bob said softly.

The Porp started bouncing anxiously, "Oh sir! I would greatly appreciate any advice you could give me! I would like for my restaurant to one day be THE spot for aliens to visit!"

Bob felt awkward. He was eager to get back to the quiet of his room, but also was put on the spot and didn't want to be rude.

"Uhh, well." Bob hesitated, "I'm here in Lorp on work. Could I maybe, uh, visit again once I'm done with my job? I could maybe give you some tips, then?" Bob internally kicked himself. He could have just said no or something simple, but instead he had to give his *future-self* extra work to do and to worry about.

The Porp bounced up and down excitedly. "Oh! That sounds great!" He was excited at having an appointment with an alien! "You can come eat here anytime for free, sir, as repayment! My name is FA-FA-LA-MI and I own this restaurant!"

Bob grimaced, but human facial expressions were as opaque to the Porp as listening to tones for mood was for Bob. Clearly, he could see their BODY language, but when they chirped it all sounded melodic, and when they spoke it all sounded gravelly.

FA-FA-LA-MI happily bobbed up and down as he returned to the kitchen. Bob turned or Ornna. "Can we get one thing before heading back to the suite?" He asked.

"Sure, anything" Ornna said.

"I need some bandages for my back after being carried around so much."

-----

![Cell](images/Cell.jpg)

In the morning, Ornna flew Bob to the holding facility where Jormmy was being held. The building was at the ground level, and only one story high, longer than it was tall. They were escorted around the building by a Porp guard called Lorly. They started with the cell.

Bob inspected each corner of the cell, tightly woven together with metallic cables, possibly imported from offworld. Bob lodged his fingers between two cables and pulled down. They were very taught and barely budged, but he could see that behind this wall was an additional wall of woven cables. He knelt down and inspected the floor and the same was true there. Overall, the cell was barren. There was a pile of straw in the corner for Jormmy to rest on, but it looked undisturbed.

"What was Jormmy being held for?" Bob inquired as he inspected the room.

"Jormmy was an accomplice to another criminal, Grorta. Jormmy would lure Porps away from their homes, and Grorta would sneak in and lay her own eggs with the eggs of the victims." Lorly explained.

"Is Grorta in custody?"

"Yes, she is also being held here. She has not escaped."

"We will have to pay her a visit, then."

Bob scribbled some notes into his notebook about the state of the room, for future reference.

- DOUBLE MESH WALLS AND FLOOR, UNLIKELY TO HAVE BENT OR MANIPULATED.
- STRAW BEDDING UNUSED. SHIFTED STRAW AROUND, NO SIGN OF TAMPERING UNDERNEATH.
- NO SIGN OF OUTSIDE ELEMENTS WITHIN THE CELL.

Lorly led them out of the cell, and another guard pulled the door closed with its beak, and pulling down a metal pole to lock the door in place.

Outside of the cells the rooms were made of the usual woven sticks and straw floors. The Porps working here bounced around as there was not much room to fly. Perhaps it was better at keeping criminals *in*, but it also meant that the guards wouldn't be very swift in a chase.

![Grorta](images/Grorta.jpg)

Bob, Lorly, and Ornna entered Grorta's cell, the guard shutting and locking the door behind them. Grorta was huddled down, sitting in her straw-pile. Her feathers were unkempt and messy. She did not turn around when the three entered her cell.

