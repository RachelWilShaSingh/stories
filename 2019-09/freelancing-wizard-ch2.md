Dama was waving her wand around aimlessly. She was standing in the middle of a *Duthahoth* - A healing place, where sick villagers go to try to have their ailments taken care of. There were other wizards in the room, working with patients or brewing potions or just doing general upkeep that needs to happen in such a place. Dama, though, was only *pretending* to work.

It was a long job, and she'd probably been on it for a few months now. Sure, it was steady money, but overall she had been hired for a pointless task, and she wasn't given the proper tools for the job - you can only purchase and maintain so many magical implements of your own when you're a freelancer. She was quite tired, awfully frustrated, and in general just did not feel up to the task at hand for the hour. She waved her wand in front of some magical parchment, appearing to be enchanting the parchments, but in reality she was just enchanting a *smaller piece of parchment* she hid under the one she was to be working on.

"Dama, can we talk for a moment?" RawothDama was waving her wand around aimlessly. She was standing in the middle of a *Duthahoth* - A healing place, where sick villagers go to try to have their ailments taken care of. There were other wizards in the room, working with patients or brewing potions or just doing general upkeep that needs to happen in such a place. Dama, though, was only *pretending* to work.

It was a long job, and she'd probably been on it for a few months now. Sure, it was steady money, but overall she had been hired for a pointless task, and she wasn't given the proper tools for the job - you can only purchase and maintain so many magical implements of your own when you're a freelancer. She was quite tired, awfully frustrated, and in general just did not feel up to the task at hand for the hour. She waved her wand in front of some magical parchment, appearing to be enchanting the parchments, but in reality she was just enchanting a *smaller piece of parchment* she hid under the one she was to be working on.

"Dama, can we talk for a moment?" Rawothá, her contact at the Duthahoth, walked up behind her. Dama put her wand down on her stack of to-be-enchanted parchments and spun around. "What's up?"

"Rawam is emphasizing that these parchments need to be done this week. Do you think you'll have them done in time?"

"Yeah, they'll be taken care of."

"OK, when do you think they'll be done, and what's your confidence level?"

"I'll probably get these finished with tomorrow." Dama, at this point, was purposely working slower than normal, because she was just so exhausted. But finishing this task would be negligible.

"Alright, alright. I have another job for you to take care of once that's done. All my other guys are busy."  Rawothá handed a ticket - a small sheet of paper with task notes on it - to Dama. Dama took the note and glanced at it; it barely had any information on it, but she knew that  Rawothá wasn't the one to ask about specifics - he didn't know, he essentially just played with the tickets all day. Sorting them, stacking them, asking everybody when they were going to get done. Professional ticket micromanager.

"Alright, I'll check it out tomorrow." Dama said, putting the ticket aside.  Rawothá continued talking to her about the state of the work at the healing place, what all needed to be done, and Rawam's mood - which greatly affected Rawothá's mood - which greatly affected Dama's mood.  Rawothá liked talking and talking and talking, but Dama was more of an "absorbs-information-by-reading" type of person. Still, there was nothing she could do but passively listen to  Rawothá, nod and give the usual social cues for "I'm still listening", and wait for him to finish up and leave her be.

"I should really charge extra for each time I'm interrupted." Dama thought to herself.

----

Alehale was out when Dama got home, but this was starting to become normal. Dama plopped herself on the rug and curled up with her cat - already sleeping on the floor - and took a short nap. After her rest, she contemplated what to do with the rest of her day, before beginning all over again the next. It was hard to decide, however, since that feeling of exhaustion was still present, physically and emotionally. She was like a cup of cold water, and her work was like a tea bag, and all that fatigue had been steeping until she had both lost her warmth and become indiscernable from the fatigue itself.

Oh, but a cup of tea sounded alright.

Dama heated up some water for herself and some milk for her cat, and sat on her stoop outside, listening to the wind and watching the trees. The day quickly came to an end and it was time to sleep and do the same thing the next day.
