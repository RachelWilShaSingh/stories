# Láadan Writings

Grammar topics covered...

| Concept                   | Which Zine | Which story |
|---------------------------+------------+-------------|
| Sounds                    |            |             |
| Basic sentence form       |          1 |           1 |
| Speech act morphemes      |          1 |         1-6 |
| Evidence morhpemes        |          1 |         1-6 |
| Tense                     |          1 |           3 |
| Adjectives                |          1 |           2 |
| Plurals                   |            |             |
| Multiple verbs            |          1 |           5 |
| Goal marker               |          1 |             |
| Source marker             |          1 |             |
| Association marker        |          1 |             |
| Beneficiary marker        |            |             |
| Instrument marker         |            |             |
| Location marker           |          1 |             |
| Manner marker             |            |             |
| Reason marker             |            |             |
| Purpose marker            |            |             |
| To-cause-to marker        |          1 |             |
| Path marker               |          1 |             |
| Duration marker           |          1 |             |
| Repetition marker         |            |             |
| Ways to perceive          |            |             |
| Possession                |            |             |
| Relationships             |            |             |
| Degree markers            |            |             |
| Noun declensions          |            |             |
| States of Consciousness   |            |             |
| Who, what, when, where... |            |             |
| Embedding sentences       |            |             |
| Passive voice             |            |             |
| Numbers and Quantity      |            |             |
| If ... Then               |            |             |

-----

## 1. Omá i Bedihá (The Teacher & The Student)

Basic sentence structure

| Láadan              | English              |
| ------              | -------              |
| Bíi omá le wa.      | I am a teacher.      |
| Bíi om le wa.       | I am teaching.       |
| Bíi ma bedihá wa.   | The student listens. |
| Bíi bedi bedihá wa. | The student learns.  |

## 2. Woshane Worul (Furry Cat)

Questions and adjectives

| Láadan                             | English                   |
| ------                             | -------                   |
| Báa híya rul?                      | Is the cat small?         |
| Ra, bíi híya ra rul wi.            | No, the cat is not small. |
| Báa rahíya rul?                    | Is the cat big?           |
| Em, bíi rahíya rul wi.             | Yes, the cat is big.      |
| Báa shane rul?                     | Is the cat furry?         |
| Em, bíi shane rul wi.              | Yes, the cat is furry.    |
| Bíi rahíya rul wi.                 | The cat is big.           |
| Bíi shane rul wi.                  | The cat is furry.         |
| Bíi rahíya i shane rul wi.         | The cat is big and furry. |
| Bíi worahíya, woshane worul be wi. | It is a big, furry cat.   |

## 3. Belidéthe (Housecleaning)

Requests and tense

| Láadan                    | English                            |
| ------                    | -------                            |
| Báa eril éthe ne belideth | Did you clean the house?           |
| Em. Bíi eril éthe le wa.  | Yes, I cleaned.                    |
| Bíi éthe ra belid wa.     | The house is not clean.            |
| Mudahéthe belid.          | The house is pig-clean.            |
| Bóo ril éthe ne belideth. | Please clean the house now.        |
| Bé aril éthe le.          | I promise that I will clean later. |

## 4. Hal i Ulanin (Work and Study)

Promises, plurals, and with

| Láadan                      | English                                  |
| ------                      | -------                                  |
| Báa shub ne bebáath?        | What are you doing?                      |
| Bíi mehal Ruth i le wa.     | Ruth and I are working.                  |
| Báa aril hal ne?            | Will you be working later?               |
| Ra, bé aril mehalra lezh.   | No, I promise we won't be working later. |
| Bóo aril ulanin leden.      | Please study with me later.              |
| Em. Bé aril mehulanin lezh. | Sure. I promise we'll study later.       |

## 5. Yobehoth (Café)

Multiple verbs, source and goal markers, "here" and "there"

| Láadan                         | English                  |
| ------                         | -------                  |
| Bíi aril sháad le yobehothedi. | I will go to the café.   |
| Báa néde sháad ne núudi?       | Do you want to go there? |
| Em, bíi néde le yobeth wa.     | Sure, I want a coffee.   |
| Bé aril merilin lezh yobeth!   | We will drink coffee!    |
| Bóo ril mesháad lezh nude!     | Let's leave (from here)! |

## 6. Raháana (Awake)

To try to, to cause to, goal marker, duration marker

| Láadan                     | English                         |
| ------                     | -------                         |
| Bíi duháana le wa.         | I am trying to sleep.           |
| Izh, thad ra le.           | But, I cannot.                  |
| Eril dóraháana rul leth.   | The cat caused-me-to-not-sleep. |
| Eril sháad le dahanedi.    | I left the bed.                 |
| Eril ban le yodeth ruledi. | I gave food to the cat.         |
| Izh ril nádelishe be.      | But she continues to cry.       |
| NÉDE ÁANA LE.              | I WANT TO SLEEP.                |

------



## 7. 


