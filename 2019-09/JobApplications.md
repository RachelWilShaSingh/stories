# Job Applications

Based on news article: https://www.telegraph.co.uk/news/2019/09/27/ai-facial-recognition-used-first-time-job-interviews-uk-find/

> The algorithms select the best applicants by assessing their performances in the videos against about 25,000 pieces of facial and linguistic information compiled from previous interviews of those who have gone on to prove to be good at the job.

----

```
APPLICANT CACHE UPDATED

102 APPLICANTS IN LAST 30 DAYS
43 APPLICANTS PASSED PROGRAMMING PRE-CHECK
21 APPLICANTS PASSED BACKGROUND PRE-CHECK
11 APPLICANTS PASSED SOCIAL MEDIA PRE-CHECK
4 APPLICANTS PASSED FACIAL SCAN PRE-CHECK

PRINT?
1. Pre-approved applicants
2. All applicants
3. Cancel

> 1

----------------------------------------------
APPLICANT b55fee06-0f27-46be-a667-6126c36c1c0b

NAME [HIDDEN FOR FAIR EVALUATION PURPOSES]
GENDER [HIDDEN FOR FAIR EVALUATION PURPOSES]
RACE [HIDDEN FOR FAIR EVALUATION PURPOSES]

PROGRAMMING PRE-CHECK... 76%    ACCEPTABLE
BACKGROUDN PRE-CHECK... CLEAR
SOCIAL MEDIA PRE-CHECK... CLEAR
- NO PUBLIC SOCIAL MEDIA ACCOUNTS AVAILABLE
FACIAL SCAN PRE-CHECK... 20%
- Results: Facial scan of applicant only within 20% match of
  skilled programer face datasets; may not be the best match.

----------------------------------------------
APPLICANT 8a26f2c0-f0a2-4e98-bc8c-5f2e1a90b919

NAME [HIDDEN FOR FAIR EVALUATION PURPOSES]
GENDER [HIDDEN FOR FAIR EVALUATION PURPOSES]
RACE [HIDDEN FOR FAIR EVALUATION PURPOSES]

PROGRAMMING PRE-CHECK... 86%    GOOD
BACKGROUDN PRE-CHECK... CLEAR
SOCIAL MEDIA PRE-CHECK... 3 WARNINGS
- Multiple keywords found in public posts including words: "Union", "Strike", "Scab"
- Multiple keywords found in public posts including words: "Human Rights", "Social Justice"
- Multiple keywords found in public posts including words: "Marx", "Communism", "Socialism", "Anarchy"
- Results: Keyword search reveals non-ideal candidate;
  may cause strife within company hierarchy, or be disobedient.
- Chance of unionization attempt: 56% WARNING
FACIAL SCAN PRE-CHECK... 60%
- Results: Facial scan of applicant within 60% match of
  skilled programer face datasets; may be a skilled programmer.
  
NEXT PAGE? (Y/N): _
```



